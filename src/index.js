const express = require('express');
var fs = require('fs');
var http = require('http');
var https = require('https');
const app = express();
const db = require('./persistence');
const getItems = require('./routes/getItems');
const addItem = require('./routes/addItem');
const updateItem = require('./routes/updateItem');
const deleteItem = require('./routes/deleteItem');

var privateKey  = fs.readFileSync('./src/cert/yourdomain.key', 'utf8');
var certificate = fs.readFileSync('./src/cert/yourdomain.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};



app.use(express.json());
app.use(express.static(__dirname + '/static'));

app.get('/items', getItems);
app.post('/items', addItem);
app.put('/items/:id', updateItem);
app.delete('/items/:id', deleteItem);


db.init().then(() => {
    var httpServer = http.createServer(app);
    var httpsServer = https.createServer(credentials, app);
    httpServer.listen(3000, () => console.log('Listening on port 3000'));
    httpsServer.listen(8443, () => console.log('Listening on port 8443'));
}).catch((err) => {
    console.error(err);
    process.exit(1);
});

const gracefulShutdown = () => {
    db.teardown()
        .catch(() => {})
        .then(() => process.exit());
};

process.on('SIGINT', gracefulShutdown);
process.on('SIGTERM', gracefulShutdown);
process.on('SIGUSR2', gracefulShutdown); // Sent by nodemon




// apt-get update && \
//     apt-get install -y openssl && \
//     openssl genrsa -des3 -passout pass:x -out server.pass.key 2048 && \
//     openssl rsa -passin pass:x -in server.pass.key -out server.key && \
//     openssl req -new -key server.key -out server.csr \
//         -subj "/C=UK/ST=Warwickshire/L=Leamington/O=OrgName/OU=IT Department/CN=example.com" && \
//     openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

//     apt-get update && \
//     apt-get install -y openssl && \
//     openssl req -new \
// -newkey rsa:2048 -nodes -keyout yourdomain.key \
// -out yourdomain.csr \
// -subj "/C=US/ST=Utah/L=Lehi/O=Your Company, Inc./OU=IT/CN=yourdomain.com" &&
// openssl x509 -req -days 365 -in yourdomain.csr -signkey yourdomain.key -out yourdomain.crt


